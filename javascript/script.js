var MainArray = []
var SecondArray = []
var ThirdArray = []
var SeatType = ["A", "B", "C", "D"]
var InputSpliter = ""
for (var i = 0; i < SeatType.length; i++) {
    for (var b = 1; b <= 8; b++) {
        InputSpliter += String(SeatType[i] + b);

    }
}
SecondArray = (String(InputSpliter.match(/[\s\S]{1,2}/g) || []).split(","));
// ThirdArray = SecondArray

// var seat = ["A1","A2","A3","A4","A5","A6","A7","A8","B1","B2","B3","B4","B5","B6","B7","B8"]
var firsttime = 0
var output = ""

function click(btn) {
    var selectedseat = []
    selectedseat = btn.value;
    var InputSpliter = (output.match(/[\s\S]{1,2}/g) || [])
    MainArray = (String(InputSpliter).split(","));
    var truth = false
    for (var i = 0; i < MainArray.length; i++) {
        if (selectedseat == MainArray[i]) {
            document.getElementById(MainArray[i]).checked = true;

            truth = false
            break;

        } else if (selectedseat !== MainArray[i]) {
            truth = true
        }
    }
    if (truth == true) {
        output += selectedseat;
    }
    var result = output;
    document.getElementById('choosen').value = (result.match(/[\s\S]{1,2}/g) || [])
}

function Check() {

    var InputSpliter = (output.match(/[\s\S]{1,2}/g) || [])
    MainArray = (String(InputSpliter).split(","))

    if (MainArray.length == 0 || MainArray[0] == "") {
        alert("please make a selection first");
    } else {


        for (var i = 0; i < MainArray.length; i++) {
            document.getElementById(MainArray[i]).disabled = true
        }


        if (document.getElementById('name').value == "") {
            document.getElementById('name').value = prompt("Please Enter Your Name", "Samuel Ling Jun Yan")
        }
        var price = parseInt(document.getElementById('PricePerTicket').value);
        document.getElementById('Price').value = price * MainArray.length;
    }

}

function Clear() {
    var InputSpliter = (output.match(/[\s\S]{1,2}/g) || [])
    MainArray = (String(InputSpliter).split(","))
    for (var i = 0; i < SecondArray.length; i++) {
        document.getElementById(SecondArray[i]).disabled = false
        document.getElementById(SecondArray[i]).checked = false
    }

    document.getElementById('choosen').value = "";
    MainArray = [];
    InputSpliter = [];
    output = "";
    document.getElementById('Price').value = "";
    document.getElementById('name').value = "";
}

function ClearCurrent() {
    var InputSpliter = (output.match(/[\s\S]{1,2}/g) || [])
    MainArray = (String(InputSpliter).split(","))
    for (var i = 0; i < MainArray.length; i++) {
        document.getElementById(MainArray[i]).disabled = false
        document.getElementById(MainArray[i]).checked = false
    }
    document.getElementById('choosen').value = "";
    MainArray = [];
    InputSpliter = [];
    output = "";
    document.getElementById('Price').value = "";
    document.getElementById('name').value = "";
}

function Next() {
    if (document.getElementById('choosen').value == "" || output == "" || document.getElementById('Price').value == "" || document.getElementById('name').value == "") {
      alert("There's a error in the form, please check if you had already filled the form")
    }
    else {

    document.getElementById('choosen').value = "";
    MainArray = [];
    InputSpliter = [];
    output = "";
    document.getElementById('Price').value = "";
    document.getElementById('name').value = "";
  }
}
